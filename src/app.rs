use eframe::{egui, epi};
use native_dialog::{FileDialog};

pub struct RustEdit {
    text: String,
    file: String,
}

impl Default for RustEdit {
    fn default() -> Self {
        Self {
            text: "".to_owned(),
            file: "".to_owned()
        }
    }
}

impl epi::App for RustEdit {
    fn update(&mut self, ctx: &egui::CtxRef, frame: &mut epi::Frame<'_>) {
        let Self { text, file } = self;

        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                egui::menu::menu(ui, "File", |ui| {
                    if ui.button("Save to file").clicked() {
                        let _ = std::fs::write(file.clone(), text.clone()); // clone hell woohoo!
                    }

                    if ui.button("Open").clicked() {
                        let path = FileDialog::new()
                            .set_location("~/")
                            .show_open_single_file()
                            .unwrap();

                        let ret = std::fs::read_to_string(path.clone().unwrap()).unwrap();
                        *text = ret;
                        *file = path.unwrap().into_os_string().into_string().unwrap();
                    }

                    if ui.button("Quit").clicked() {
                        frame.quit();
                    }
                });
            });
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            egui::ScrollArea::auto_sized().show(ui, |ui| {
                ui.add(
                    egui::TextEdit::multiline(text)
                        .desired_rows(39) // cry about it
                        .lock_focus(true)
                        .desired_width(f32::INFINITY)
                        .code_editor()
                );
            });
        });
    }

    fn name(&self) -> &str { "Rust Edit" }
}